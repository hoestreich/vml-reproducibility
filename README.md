# VML-reproducibility

Reproducibility Evaluation of NeurIPS 2019 Paper "Composable Specification Language for ReinforcementLearning Tasks" by Kishor Jothimurugan, Rajeev Alur and Osber Bastani.
Contains the results of our executed test runs and a jupyter notebook for viz.